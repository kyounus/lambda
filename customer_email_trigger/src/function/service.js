const { info, debug, errorInfo } = require('../constants/console')
const CTService = require('../service/CTService')
const {
    CT_SENDGRID_EMAIL,

  } = require('../constants/httpUri')

// const displayJson = (data) => ((data) ? JSON.stringify(data) : 'Not Available')
const viewJSON = (data) => JSON.stringify(data)

// const handleError = async (error, product) => {
//   errorInfo(MIGRATION_FAILED)
//   errorInfo(`Product -> ${displayJson(product)}`)
//   const snsPayload = {
//       error: MIGRATION_FAILED,
//       payload: {
//           product,
//       },
//   }
//   await repository.snsMessage(EXCEPTION_SNS, snsPayload, false)
//   throw error
// }

const sendEmailtoCustomer = async (payload) => {
  const url = `${CT_SENDGRID_EMAIL}`
  const token = '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'

  const res = await CTService.emailEntity(url, payload, token)
  console.log('email response', res)
  // return res;
}

const processMessages = async (messages) => {
  info('Process Messages')
  for await (const message of messages) {
    debug(`Message -> ${message}`)
    // if (await validate(message, token)) {
    //     await transform(message, token)
    // }
    await sendEmailtoCustomer(message)
}
}

const getMessages = (event) => {
  info('Fetch Messages')
  const { Records } = event
  const messages = []
  if (Records) {
      for (const record of Records) {
          try {
              const { body } = record
              const message = JSON.parse(body)
              messages.push(message)
          } catch (error) {
              errorInfo(`Invalid Message format ${error}`)
          }
      }
  }
  debug(`Messages -> ${viewJSON(messages)}`)
  return messages
}

module.exports = {
  getMessages,
  processMessages,
}
