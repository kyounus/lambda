const _ = require('lodash')
const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const expect = chai.expect
chai.use(sinonChai)
const sqsMessage = require('../../../src/handler/sqs')
const AWS = require('aws-sdk')

describe('SQS', () => {
    describe('Message', () => {
        it('handle lambda invokation call with valid payload', async () => {
            const sqs = new AWS.SQS()
            const requestPayload = {
                cartId: 'test-cart-id'
            }
            const payload = {
                QueueUrl: `test_sqs_url_dev`,
                MessageBody: JSON.stringify(requestPayload),
            }
            
            sqsMessage.message(sqs, payload)
                .then(response => sinon.assert.pass())
                .catch(error => sinon.assert.fail())
        })
    })

    before(() => {
        process.env.LOG_MODE = 'INFO'
    })
    
    after(() => {
        process.env.LOG_MODE = 'DEBUG'
    })

})