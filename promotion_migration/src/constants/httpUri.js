module.exports = {
  CT_OAUTH_TOKEN: '/oauth/token',
  CT_CART_DISCOUNTS: '/cart-discounts',
  CT_CART_DISCOUNT_CODE: '/discount-codes',
}
