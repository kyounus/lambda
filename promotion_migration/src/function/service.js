const { uuid } = require('uuidv4')
const { info, errorInfo } = require('../constants/console')
const repository = require('./repository')
const CTService = require('../service/CTService')
const discountConditonMapper = require('../constants/discountConditonMapper')
const { CT_CART_DISCOUNTS } = require('../constants/httpUri')
const { MIGRATION_FAILED } = require('../constants/message')

const { EXCEPTION_SNS } = process.env

// DECODE LOGIC from Magento to CT
let ROOT_AGGREGATOR = ''
let decodedRule = '('
let inc = 0
let CONDITIONS_LENGTH = 0
let CONDITION_COUNTER = 0

const formRule = (magentoRule, subConditionFlag, subCondLength) => {
  let subConditionAggregator = ''
  let setLength = 0
  let subInc = 0
  // Getting the length of all conditions length to close the last bracket
  if (magentoRule[0].conditions && CONDITIONS_LENGTH === 0) {
    CONDITIONS_LENGTH = magentoRule[0].conditions.length
  }
  // checking the condition property, if
  if (CONDITIONS_LENGTH === 0) {
    decodedRule = discountConditonMapper.appliesToAll
    return
  }
  magentoRule.forEach((magentoRuleCond) => {
    // Root Aggregator to set in variable
    if (!ROOT_AGGREGATOR) {
      ROOT_AGGREGATOR = ` ${discountConditonMapper.aggregator[magentoRuleCond.aggregator]} `
    }
    // Set the Opening Bracket
    if (magentoRuleCond.aggregator && !magentoRuleCond.type.includes('Combine')) {
      inc += 1
      decodedRule += '('
    }
    // Set the sub condition rule
    if (magentoRuleCond.attribute) {
      let value
      if (magentoRuleCond.attribute === 'qty' || magentoRuleCond.attribute === 'quote_item_qty') {
        value = `${magentoRuleCond.value}`
      } else if (magentoRuleCond.operator === '()') {
        const splittedValue = magentoRuleCond.value.split(',')
        let formatted = ''
        const { length } = splittedValue
        splittedValue.map((t, i, a) => {
          console.log(a)
          formatted += `"${t}"${(i < length - 1) ? ',' : ''}`
        })
        value = `(${formatted})`
      } else {
        value = `"${magentoRuleCond.value}"`
      }
      decodedRule += `${discountConditonMapper.actions[magentoRuleCond.attribute]} ${discountConditonMapper.operator[magentoRuleCond.operator]} ${value}`
      // decodedRule = decodedRule.replace(/"[0-9]"/g, `${temp}`)
    }
    if (magentoRuleCond.aggregator && magentoRuleCond.attribute && magentoRuleCond.type.includes('Subselect') && (magentoRuleCond.conditions && magentoRuleCond.conditions.length > 1)) {
      inc += 1
      decodedRule += ` ${discountConditonMapper.aggregator.all} `
      decodedRule += '('
      subConditionAggregator = magentoRuleCond.aggregator
      setLength = magentoRuleCond.conditions.length
    } else if (magentoRuleCond.aggregator && magentoRuleCond.attribute && magentoRuleCond.type.includes('Subselect') && magentoRuleCond.conditions) {
      decodedRule += ` ${discountConditonMapper.aggregator[magentoRuleCond.aggregator]} `
    }

    if (magentoRuleCond.conditions) {
      formRule(magentoRuleCond.conditions, subConditionAggregator, setLength)
    }
    subInc += 1
    if (magentoRuleCond.type.includes('Product') && subConditionFlag && subInc !== subCondLength) {
      decodedRule += ` ${discountConditonMapper.aggregator[subConditionFlag]} `
    }
    if (magentoRuleCond.type.includes('Subselect') && subInc === 0 || magentoRuleCond.type.includes('Combine') && subInc === 0
            || magentoRuleCond.type.includes('Subselect') && inc !== 0 || magentoRuleCond.type.includes('Combine') && inc !== 0) {
      decodedRule += ')'
    } else if (inc !== 0 && !magentoRuleCond.aggregator && !subConditionFlag) {
      decodedRule += ')'
    }

    if (inc === 1 || inc === 0 && !magentoRuleCond.conditions) {
      const counter = CONDITION_COUNTER += 1
      if (counter !== CONDITIONS_LENGTH) {
        decodedRule += `${ROOT_AGGREGATOR}`
      }
    }

    if (magentoRuleCond.type.includes('Combine')) {
      decodedRule += ')'
    }
    if (inc !== 0 && !subConditionFlag) {
      inc -= 1
    }
  })
}

const decodeConditions = async (condObj) => {
  info('PROMOTION MIGRATION HANDLER - decodeConditions')
  let conditionObject = condObj
  const regex = /\\/g
  conditionObject = conditionObject.replace(regex, '\\\\')
  try { conditionObject = JSON.parse(`[${conditionObject}]`) } catch (e) { conditionObject = JSON.parse(`[${conditionObject.replace(/"attribute_scope":"}/g, '"attribute_scope":""}')}]`) }
  formRule(conditionObject)
  inc = 0
  CONDITIONS_LENGTH = 0
  CONDITION_COUNTER = 0
  ROOT_AGGREGATOR = ''
  decodedRule = decodedRule.replace(/"/g, '\"')
  console.log('- - - - - - - - - - - - - ->', decodedRule)
  return decodedRule
}

const handleError = async (error, promotion) => {
  errorInfo(MIGRATION_FAILED)
  const snsPayload = {
    error: MIGRATION_FAILED,
    payload: {
      promotion,
    },
  }
  await repository.snsMessage(EXCEPTION_SNS, snsPayload, false)
  throw error
}

const createCartDiscount = async (data, token) => {
  try {
    const cartDiscountPayload = {
      name: {
        en: (data[discountConditonMapper.CART_DISCOUNT_KEYS.NAME]) ? data[discountConditonMapper.CART_DISCOUNT_KEYS.NAME] : '',
      },
      key: (data[discountConditonMapper.CART_DISCOUNT_KEYS.KEY]) ? `${data[discountConditonMapper.CART_DISCOUNT_KEYS.KEY]}_${uuid()}` : uuid(),
      sortOrder: new String(Math.random()),
      isActive: false,
      requiresDiscountCode: true,
    }
    if (data[discountConditonMapper.CART_DISCOUNT_KEYS.DISCOUNT_TYPE]) {
      if ((data[discountConditonMapper.CART_DISCOUNT_KEYS.DISCOUNT_TYPE] === 'by_fixed'
                || data[discountConditonMapper.CART_DISCOUNT_KEYS.DISCOUNT_TYPE] === 'cart_fixed')) {
        cartDiscountPayload.value = {
          type: 'absolute',
          money: [{
            currencyCode: 'USD',
            centAmount: (data[discountConditonMapper.CART_DISCOUNT_KEYS.DISC_AMOUNT])
              ? parseInt(data[discountConditonMapper.CART_DISCOUNT_KEYS.DISC_AMOUNT] * 100) : 0,
          }],
        }
      } else {
        cartDiscountPayload.value = {
          type: 'relative',
          permyriad: (data[discountConditonMapper.CART_DISCOUNT_KEYS.DISC_AMOUNT])
            ? parseInt(data[discountConditonMapper.CART_DISCOUNT_KEYS.DISC_AMOUNT] * 100) : 0,
        }
      }
    }
    if (data[discountConditonMapper.CART_DISCOUNT_KEYS.CART_PREDICATE]) {
      decodedRule = '('
      let updatedRule = '1=1' // await decodeConditions(data[discountConditonMapper.CART_DISCOUNT_KEYS.CART_PREDICATE])
      if (updatedRule !== '1=1') {
        updatedRule = `forAllLineItems(${updatedRule}) = true`
      }
      cartDiscountPayload.cartPredicate = updatedRule
    }
    if (data[discountConditonMapper.CART_DISCOUNT_KEYS.TARGET]) {
      decodedRule = '('
      cartDiscountPayload.target = { type: 'lineItems', predicate: await decodeConditions(data[discountConditonMapper.CART_DISCOUNT_KEYS.TARGET]) }
    }
    if (data[discountConditonMapper.CART_DISCOUNT_KEYS.FROM_DATE] !== 'NULL') {
      cartDiscountPayload.validFrom = new Date(
        data[discountConditonMapper.CART_DISCOUNT_KEYS.FROM_DATE],
)
    }
    if (data[discountConditonMapper.CART_DISCOUNT_KEYS.TO_DATE] !== 'NULL') {
      cartDiscountPayload.validUntil = new Date(
        data[discountConditonMapper.CART_DISCOUNT_KEYS.TO_DATE],
)
    }
    const cartDiscountResponse = await CTService
    .createEntity(CT_CART_DISCOUNTS, cartDiscountPayload, token)
    if (cartDiscountResponse.errors) {
      return cartDiscountResponse.errors
    }
    // const cartDiscountCodePayload = {
    //     code: (data[discountConditonMapper.CART_DISCOUNT_KEYS.NAME]) ?
    // data[discountConditonMapper.CART_DISCOUNT_KEYS.NAME] : '',
    //     name: {
    //         en: (data[discountConditonMapper.CART_DISCOUNT_KEYS.NAME]) ?
    // data[discountConditonMapper.CART_DISCOUNT_KEYS.NAME] : ''
    //     },
    //     cartDiscounts: [{
    //         typeId: "cart-discount",
    //         id: cartDiscountResponse.id
    //     }],
    //     isActive: true,
    //     cartPredicate: "1=1",
    //     maxApplications:
    // (data[discountConditonMapper.CART_DISCOUNT_KEYS.USES_PER_COUPON] != '0') ?
    // parseFloat(data[discountConditonMapper.CART_DISCOUNT_KEYS.USES_PER_COUPON]) : 0,
    //     maxApplicationsPerCustomer:
    // (data[discountConditonMapper.CART_DISCOUNT_KEYS.USES_PER_CUSTOMER] != '0') ?
    // parseFloat(data[discountConditonMapper.CART_DISCOUNT_KEYS.USES_PER_CUSTOMER]) : 0

    // }

    // const cartDiscountCode = await CTService.
    // createEntity(CT_CART_DISCOUNT_CODE, cartDiscountCodePayload, token)

    // if (cartDiscountCode.errors) {
    //     return cartDiscountCode.errors
    // }
    return true
  } catch (e) {
    return e
  }
}

const processData = async (data, token) => {
  const PROMOTION_ERRORS = {}
  await Promise.all(data.map(async (element) => {
    const cartDiscountCreationSubmitResponse = await createCartDiscount(element, token)
    if (cartDiscountCreationSubmitResponse) {
      PROMOTION_ERRORS[element[discountConditonMapper
      .CART_DISCOUNT_KEYS.NAME]] = cartDiscountCreationSubmitResponse
    }
  }))
  return PROMOTION_ERRORS
}

const getS3Data = async (event) => {
  const bucket = event.Records[0].s3.bucket.name
  const { key } = event.Records[0].s3.object
  const bucketDetails = {
    Bucket: bucket,
    Key: key,
  }
  try {
    const bucketResponse = await repository.s3Message(bucketDetails)
    return bucketResponse
  } catch (err) {
    console.log(err)
    await handleError(err)
    return err
  }
}

const syncData = async () => {
  // Query Discounts
  const DISCOUNT_ERRORS = {}
  const token = await CTService.getToken()
  const cartDiscounts = await CTService.getEntity(`${CT_CART_DISCOUNTS}?limit=500`, token)
  cartDiscounts.results.forEach(async (cartDiscount) => {
    // delete cartDiscount.id
    // delete cartDiscount.version
    // delete cartDiscount.createdAt
    // delete cartDiscount.lastModifiedAt
    // delete cartDiscount.lastModifiedBy
    // delete cartDiscount.createdBy
    // delete cartDiscount.stackingMode
    // delete cartDiscount.references
    // delete cartDiscount.attributeTypes
    // delete cartDiscount.cartFieldTypes
    // delete cartDiscount.lineItemFieldTypes
    // delete cartDiscount.customLineItemFieldTypes
    // let updatedCartDiscount = cartDiscount
    let updatedCartPredicate = cartDiscount.cartPredicate
    let updatedCartTarget = cartDiscount.target.predicate
    if (cartDiscount.cartPredicate !== '1=1') {
      for (const skuMap in discountConditonMapper.SKU_MAP) {
        if (cartDiscount.cartPredicate.includes(skuMap)) {
          updatedCartPredicate = updatedCartPredicate
            .replace(skuMap, discountConditonMapper.SKU_MAP[skuMap])
        }
      }
      // updatedCartPredicate = updatedCartPredicate.replace(/"/g, '\\"')
    }

    if (cartDiscount.target) {
      for (const skuMap in discountConditonMapper.SKU_MAP) {
        if (cartDiscount.target.predicate.includes(skuMap)) {
          updatedCartTarget = updatedCartTarget
            .replace(skuMap, discountConditonMapper.SKU_MAP[skuMap])
        }
      }
      // updatedCartTarget = updatedCartTarget.replace(/"/g, '\\"')
    }
    console.log('---- >updatedCartPredicate', updatedCartPredicate)
    console.log('---- >updatedCartTarget', updatedCartTarget)
    const updateDiscountPayload = {
      version: cartDiscount.version,
      actions: [
        {
          action: 'changeCartPredicate',
          cartPredicate: updatedCartPredicate,
          predicate: updatedCartTarget,
        },
      ],
    }
    console.log('---->updateDiscountPayload', JSON.stringify(updateDiscountPayload))
    const updatedCartDiscountResponse = await CTService.createEntity(`${CT_CART_DISCOUNTS}/${cartDiscount.id}`,
      updateDiscountPayload, token)
    console.log('---->updatedCartDiscountResponse --->', updatedCartDiscountResponse)
    if (updatedCartDiscountResponse.errors) {
      DISCOUNT_ERRORS[cartDiscount.name['en-US']] = updatedCartDiscountResponse
    }
    return DISCOUNT_ERRORS
  })
  return cartDiscounts
  // loop discount and append data
  // create discount
}

const processMessage = async (event) => {
  try {
    let response = {}
    if (event.httpMethod === 'GET') {
      response = await syncData()
    } else {
      const s3Data = await getS3Data(event)
      const token = await CTService.getToken()
      response = await processData(s3Data, token)
    }
    if (response) {
      console.log('- - - - - - >response', response)
      // await fs.writeFile('filename.json', response);
      // await handleError(response)
    }
  } catch (err) {
    console.log(err)
    await handleError(err)
    return err
  }
}

module.exports = {
  processMessage,
}
