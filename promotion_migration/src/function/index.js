require('custom-env').env('local')
const { info, errorInfo } = require('../constants/console')
const service = require('./service')

const handler = async (event) => {
  try {
    info('PROMOTION MIGRATION HANDLER')
    let eventData = {}
    if (process.env.LOG_MODE === 'DEBUG' && event.body) {
      eventData = JSON.parse(event.body)
    } else {
      eventData = event
    }
    await service.processMessage(eventData)

    return {}
  } catch (error) {
    errorInfo(error)
    return {}
  }
}

module.exports = {
  handler,
}
