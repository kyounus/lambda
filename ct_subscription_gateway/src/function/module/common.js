/* eslint-disable no-restricted-syntax */
const _ = require('lodash')
const { info, debug, errorInfo } = require('../../constants/console')
const repository = require('../repository')
const CTService = require('../../service/CTService')
const {
    CT_CUSTOM_OBJECT,
} = require('../../constants/httpUri')
const {
    SNS,
    SQS,
    PROCESSING_FAILED,
 } = require('../../constants/message')

const {
    EXCEPTION_SNS,
 } = process.env
require('custom-env').env(true)

const viewJSON = (data) => JSON.stringify(data)

const postData = async (type, entityName, data, aliasEnabled) => {
    if (_.isEqual(type, SNS)) {
        debug(`Payload sent to ${SNS} ${entityName} -> ${viewJSON(data)}`)
        await repository.snsMessage(entityName, data, aliasEnabled)
    } else if (_.isEqual(type, SQS)) {
        debug(`Payload sent to ${SQS} ${entityName} -> ${viewJSON(data)}`)
        await repository.sqsMessage(entityName, data, aliasEnabled)
    }
}

const handleError = async (error, data) => {
    errorInfo(`${PROCESSING_FAILED} ${data.entity} -> ${(data.object) ? JSON.stringify(data.object) : 'Not Available'}`)
    const snsPayload = data
    snsPayload.error = error
    await postData(SNS, EXCEPTION_SNS, snsPayload, false)
    throw error
}

const getObject = async (url, token) => {
    info('Get Object')
    const object = await CTService.getEntity(url, token)
    debug(`Object -> ${viewJSON(object)}`)
    return object
}

const getCustomObject = async (accountId, token) => {
    info('Get Custom Object')
    let customObj
    const predicate = `?where=id%3D%22${accountId}%22&expand=value.customer.id&expand=value.cart.id`
    const customObjects = await CTService.getEntity(`${CT_CUSTOM_OBJECT}${predicate}`, token)
    for (const customObject of customObjects.results) {
        customObj = customObject
    }
    debug(`Custom Object -> ${viewJSON(customObj)}`)
    return customObj
}

module.exports = {
    handleError,
    getObject,
    getCustomObject,
    postData,
    viewJSON,
}
