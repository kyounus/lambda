/* eslint-disable no-param-reassign */
const _ = require('lodash')
const { info, debug } = require('../../constants/console')
const common = require('./common')
const {
    CT_CUSTOMER,
} = require('../../constants/httpUri')
const {
    SNS,
    SQS,
    PAYLOAD_ACTION_UPDATE,
    ALLOWED_PAYLOAD_ATTRIBUTES,
} = require('../../constants/message')

const {
    CUSTOMER_SNS,
    NETSUITE_CUSTOMER_SQS,
} = process.env
require('custom-env').env(true)

const getChannel = (object) => {
    let channel = 'B2B'
    if (object
        && object.custom
        && object.custom.fields
        && object.custom.fields.channel) {
            channel = object.custom.fields.channel
    }
    return channel
}

const handle = async (data, token) => {
    info('Handle Customer')
    debug(`Payload -> ${common.viewJSON(data)}`)
    try {
        if (_.isEqual(data.action, PAYLOAD_ACTION_UPDATE)) {
            // Customer Update
            const url = `${CT_CUSTOMER}/${data.objectReference}`
            const customer = await common.getObject(url, token)
            data.object = customer
        } else {
            // Customer Create
            data.object = data.fullObject.customer
            // await common.postData(SNS, CT_CUSTOMER_SNS, data.fullObject, false)
        }
        await common.postData(SQS, NETSUITE_CUSTOMER_SQS, data.fullObject, false)
        data = _.pick(data, ALLOWED_PAYLOAD_ATTRIBUTES)
        data.channel = getChannel(data.object)
        await common.postData(SNS, CUSTOMER_SNS, data, false)
    } catch (error) {
        await common.handleError(error, data)
    }
}

module.exports = {
  handle,
}
