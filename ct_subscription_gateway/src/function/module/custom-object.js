/* eslint-disable no-param-reassign */
const _ = require('lodash')
const { info, debug } = require('../../constants/console')
const common = require('./common')
const {
    SNS,
    SQS,
    CONTAINER_ACCOUNT,
    CONTAINER_PAYMENT,
    B2B_SUBSCRIPTION,
    D2C_SUBSCRIPTION,
    B2B_CHANNEL,
    D2C_CHANNEL,
    CT_CUSTOM_ACCOUNT,
    CT_CUSTOM_PAYMENT,
    CT_CUSTOM_SUBSCRIPTION,
    ALLOWED_PAYLOAD_ATTRIBUTES,
 } = require('../../constants/message')

const {
    SUBSCRIPTION_SNS,
    NETSUITE_CUSTOMER_SQS,
 } = process.env
require('custom-env').env(true)

const handle = async (data, token) => {
    info('Handle Custom Object')
    debug(`Payload -> ${common.viewJSON(data)}`)
    try {
        const customObject = await common.getCustomObject(data.objectReference, token)
        if (customObject
            && customObject.container) {
                if (_.isEqual(customObject.container, CONTAINER_ACCOUNT)) {
                    data.object = customObject
                    data.entity = CT_CUSTOM_ACCOUNT
                    debug(`Account Object -> ${common.viewJSON(data)}`)
                    await common.postData(SQS, NETSUITE_CUSTOMER_SQS, data.fullObject, false)
                } else if (_.isEqual(customObject.container, CONTAINER_PAYMENT)) {
                    data.object = customObject
                    data.entity = CT_CUSTOM_PAYMENT
                    debug(`Payment Object -> ${common.viewJSON(data)}`)
                    await common.postData(SQS, NETSUITE_CUSTOMER_SQS, data.fullObject, false)
                } else if (_.startsWith(customObject.container, B2B_SUBSCRIPTION)) {
                    data.object = customObject
                    data.entity = CT_CUSTOM_SUBSCRIPTION
                    data.channel = B2B_CHANNEL
                    data = _.pick(data, ALLOWED_PAYLOAD_ATTRIBUTES)
                    await common.postData(SNS, SUBSCRIPTION_SNS, data, false)
                } else if (_.startsWith(customObject.container, D2C_SUBSCRIPTION)) {
                    data.object = customObject
                    data.entity = CT_CUSTOM_SUBSCRIPTION
                    data.channel = D2C_CHANNEL
                    data = _.pick(data, ALLOWED_PAYLOAD_ATTRIBUTES)
                    await common.postData(SNS, SUBSCRIPTION_SNS, data, false)
                }
        }
    } catch (error) {
        await common.handleError(error, data)
    }
}

module.exports = {
  handle,
}
