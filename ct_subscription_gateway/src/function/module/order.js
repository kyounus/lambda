/* eslint-disable no-restricted-syntax */
/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
const _ = require('lodash')
const { info, debug, errorInfo } = require('../../constants/console')
const common = require('./common')
const CTService = require('../../service/CTService')
const {
    CT_ORDER,
    CT_STATES,
    CT_PAYMENTS,
    CT_CART,
    CT_CUSTOM_OBJECT,
    CT_PRODUCT_TYPE,
} = require('../../constants/httpUri')
const {
    SNS,
    SQS,
    ORDER_CREATED,
    ORDER_STATE_CHANGED,
    ORDER_STATE_TRANSITION,
    ORDER_PAYMENT_STATE_CHANGED,
    ORDER_RETURN_INFO_ADDED,
    // ORDER_RETURN_SHIPMENT_CHANGED,
    LINEITEM_STATE_TRANSITION,
    LINEITEM_SHIPPED_STATE,
    LINEITEM_ACCEPTED_STATE,
    ALLOWED_PAYMENT_STATE_CHANGED,
    CONFIRMED,
    PROCESSING,
    ALLOWED_PAYLOAD_ATTRIBUTES,
    ALLOWED_ANALYTICS_PAYLOAD_ATTRIBUTES,
    OPEN,
    SUBMITTED,
    CUSTOMERHOLD,
    CANCELLED,
} = require('../../constants/message')

const {
    ORDER_SNS,
    ANALYTICS_ACH_PAID_SNS,
    CT_ORDER_VALIDATE_SQS,
    CT_ORDER_STATE_SQS,
    CT_ORDER_SHIPPED_SQS,
    NETSUITE_ORDER_SQS,
} = process.env
require('custom-env').env(true)

const orderWorkflowState = (order) => {
    const { state } = order
    let workflowState
    if (state
        && state.obj
        && state.obj.name
        && state.obj.name.en) {
            workflowState = state.obj.name.en
    }
    return workflowState
}

const getChannel = (object) => {
    let channel = 'B2B'
    if (object
        && object.custom
        && object.custom.fields
        && object.custom.fields.channel) {
            channel = object.custom.fields.channel
    }
    return channel
}

const constructAnalyticsPayload = (data) => {
    const payload = _.pick(data, ALLOWED_ANALYTICS_PAYLOAD_ATTRIBUTES)
    payload.action = 'create'
    payload.channel = getChannel(payload.object)
    return payload
}

const constructValidatePayload = (data) => {
    const payload = _.pick(data, ALLOWED_PAYLOAD_ATTRIBUTES)
    if (data
        && data.fullObject) {
            if (data.fullObject.oldOrderState) {
                payload.oldOrderState = data.fullObject.oldOrderState
            }
            if (data.fullObject.state) {
                payload.state = data.fullObject.state
            }
    }
    return payload
}

const constructPaymentStatePayload = (data) => {
    const payload = _.pick(data, ALLOWED_PAYLOAD_ATTRIBUTES)
    if (data
        && data.fullObject) {
            if (data.fullObject.paymentState) {
                payload.paymentState = data.fullObject.paymentState
            }
            if (data.fullObject.oldPaymentState) {
                payload.oldPaymentState = data.fullObject.oldPaymentState
            }
    }
    return payload
}

const constructStatePayload = (data) => {
    const payload = _.pick(data, ALLOWED_PAYLOAD_ATTRIBUTES)
    if (data
        && data.fullObject) {
            if (data.fullObject.fromState) {
                payload.fromState = data.fullObject.fromState
            }
            if (data.fullObject.toState) {
                payload.toState = data.fullObject.toState
            }
            if (data.fullObject.quantity) {
                payload.quantity = data.fullObject.quantity
            }
            if (data.fullObject.lineItemId) {
                payload.lineItemId = data.fullObject.lineItemId
            }
    }
    return payload
}

const getState = (states, stateKey) => _.find(states, (state) => _.isEqual(state.key, stateKey))

const constructPaymentTransactionPayload = (payment) => {
    const payload = {
        version: payment.version,
        actions: [],
    }
    if (payment
        && payment.transactions
        && _.isEqual(_.size(payment.transactions), 1)
        && payment.transactions[0].state
        && _.isEqual(payment.transactions[0].state, 'Pending')) {
            payload.actions.push({
                action: 'changeTransactionState',
                transactionId: payment.transactions[0].id,
                state: 'Success',
            })
    }
    return payload
}

const netsuiteLineItems = async (order, token) => {
    const subscriptions = []
    const url = `${CT_PRODUCT_TYPE}/key=subscription`
    const subscriptionType = await CTService.getEntity(url, token)
    for await (const lineItem of order.lineItems) {
        if (subscriptionType
            && subscriptionType.id
            && lineItem
            && lineItem.productType
            && lineItem.productType.id) {
                if (!_.isEqual(subscriptionType.id, lineItem.productType.id)) {
                    debug(`Netsuite Line Item -> ${lineItem.productType.id}`)
                    subscriptions.push(lineItem)
                }
        }
    }
    return subscriptions
}

const constructNetsuitePayload = async (data, token) => {
    const netsuiteData = data
    const lineItems = await netsuiteLineItems(netsuiteData.object, token)
    netsuiteData.object.lineItems = lineItems
    const payload = _.pick(netsuiteData, ALLOWED_PAYLOAD_ATTRIBUTES)
    payload.channel = getChannel(payload.object)
    payload.returnInfo = data.returnInfo
    return payload
}

const constructReturnAnalyticsPayload = (data) => {
    const payload = _.pick(data, ALLOWED_PAYLOAD_ATTRIBUTES)
    payload.channel = getChannel(payload.object)
    payload.returnInfo = data.returnInfo
    return payload
}

const updatePaymentTransactionState = async (payment, token) => {
    const url = `${CT_PAYMENTS}/${payment.id}`
    const payload = constructPaymentTransactionPayload(payment)
    debug(`Change Transaction State Payload -> ${common.viewJSON(payload)}`)
    if (payload
        && _.isEqual(_.size(payload.actions), 1)) {
            const response = await CTService.createEntity(url, payload, token)
            debug(`Updated Payment Object -> ${common.viewJSON(response)}`)
            if (response && response.id) {
                payment = response
            }
    }
}

const getUpdatedOrder = async (oldOrder, token) => {
    const url = `${CT_ORDER}/${oldOrder.id}?expand=state.id&expand=lineItems[*].state[*].state.id&expand=paymentInfo.payments[*].id`
    const order = await common.getObject(url, token)
    return order
}

const getSubscriptionCarts = async (order, token) => {
    info('Cancelled - Get Subscriptions Carts')
    let carts = []
    try {
        const url = `${CT_CART}?where=%28custom%28fields%28parentOrderReferenceId%28id%3D%22${order.id}%22%29%29%29%29`
        const response = await CTService.getEntity(url, token)
        if (response
            && response.results
            && !_.isEmpty(response.results)) {
                carts = response.results
        }
    } catch (error) {
        errorInfo(`Error -> ${error}`)
    }
    debug(`Cancelled - Subscription Carts -> ${common.viewJSON(carts)}`)
    return carts
}

const getOrderSubscriptions = async (order, token) => {
    info('Cancelled - Get Order Subscriptions')
    const subscriptions = []
    try {
        const carts = await getSubscriptionCarts(order, token)
        for await (const cart of carts) {
            const url = `${CT_CUSTOM_OBJECT}?where=value%28cart%28id%3D%22${cart.id}%22%29%20and%20state%3D%22Scheduled%22%29`
            const response = await CTService.getEntity(url, token)
            for (const subscription of response.results) {
                subscriptions.push(subscription)
            }
        }
    } catch (error) {
        errorInfo(`Error -> ${error}`)
    }
    debug(`Cancelled - Subscription -> ${common.viewJSON(subscriptions)}`)
    return subscriptions
}

const cancelSubscription = async (subscription, order, token) => {
    info('Cancelled - Cancel Subscriptions')
    try {
        const url = `${CT_CUSTOM_OBJECT}`
        subscription.value.state = CANCELLED
        subscription.value.transactions.push({
            type: 'cancelled',
            messages: `Order ${order.orderNumber} was Cancelled`,
        })
        subscription = _.pick(subscription, ['container', 'key', 'value'])
        const response = await CTService.createEntity(url, subscription, token)
        debug(`Response -> ${common.viewJSON(response)}`)
        if (response && response.id) {
            debug(`Subscription ${response.container}/${response.key} was successfully Cancelled`)
        }
    } catch (error) {
        errorInfo(`Error -> ${error}`)
    }
}

const cancelOrderSubscription = async (data, token) => {
    info('Cancel Order Subscriptions')
    const { object } = data
    try {
        const subscriptions = await getOrderSubscriptions(object, token)
        for await (const subscription of subscriptions) {
            await cancelSubscription(subscription, object, token)
        }
    } catch (error) {
        errorInfo(`Error -> ${error}`)
    }
}

const handleOrderTrigger = async (data, token) => {
    const { object } = data
    const state = orderWorkflowState(object)
    // CREATED
    if (_.isEqual(data.actionType, ORDER_CREATED)
        && (_.isEqual(state, CUSTOMERHOLD)
            || _.isEqual(state, SUBMITTED))
        && _.isEqual(object.orderState, OPEN)) {
            info('Order Created')
            await common.postData(SQS,
                CT_ORDER_VALIDATE_SQS,
                constructValidatePayload(data),
                false)
    }
    // State - Changed / Transition
    if (_.isEqual(data.actionType, ORDER_STATE_CHANGED)
        || _.isEqual(data.actionType, ORDER_STATE_TRANSITION)) {
            info('Order State Changed')
            // PROCESSING & CONFIRMED
            if (_.isEqual(state, PROCESSING)
                && _.isEqual(object.orderState, CONFIRMED)) {
                    // Analytics
                    await common.postData(SNS,
                        ORDER_SNS,
                        constructAnalyticsPayload(data),
                        false)
                    // Validate
                    await common.postData(SQS,
                        CT_ORDER_VALIDATE_SQS,
                        constructValidatePayload(data),
                        false)
            }
            // Cancelled
            if (_.isEqual(state, CANCELLED)
                && _.isEqual(data.actionType, ORDER_STATE_TRANSITION)) {
                    await cancelOrderSubscription(data, token)
                    // Analytics
                    const updatedData = _.cloneDeep(constructAnalyticsPayload(data))
                    updatedData.action = 'cancel'
                    updatedData.channel = getChannel(updatedData.object)
                    await common.postData(SNS,
                        ORDER_SNS,
                        updatedData,
                        false)
            }
    }
    // Line Item State - Transition
    if (_.isEqual(data.actionType, LINEITEM_STATE_TRANSITION)) {
        info('LineItem State Changed')
        const payload = constructStatePayload(data)
        if (payload
            && payload.toState) {
                // Accepted
                const acceptedState = getState(data.states, LINEITEM_ACCEPTED_STATE)
                if (_.isEqual(acceptedState.id, payload.toState.id)
                && !_.isEqual(acceptedState.id, payload.fromState.id)) {
                    payload.channel = getChannel(payload.object)
                    await common.postData(SNS,
                        ORDER_SNS,
                        payload,
                        false)
                }
                // Shipped
                const shippedState = getState(data.states, LINEITEM_SHIPPED_STATE)
                if (_.isEqual(shippedState.id, payload.toState.id)
                && !_.isEqual(shippedState.id, payload.fromState.id)) {
                    await common.postData(SQS,
                        CT_ORDER_SHIPPED_SQS,
                        payload,
                        false)
                }
        }
        await common.postData(SQS,
            CT_ORDER_STATE_SQS,
            payload,
            false)
    }
    // Paymenet State - Transition
    if (_.isEqual(data.actionType, ORDER_PAYMENT_STATE_CHANGED)) {
        info('Payment State Changed')
        const payload = constructPaymentStatePayload(data)
        debug(`Old State -> ${payload.oldPaymentState}`)
        debug(`State -> ${payload.paymentState}`)
        if (payload
            && payload.paymentState
            && payload.oldPaymentState) {
                if (_.isEqual(payload.paymentState, 'Paid')
                    && _.isEqual(payload.oldPaymentState, 'Pending')) {
                        info('Payment state changed from Pending to Paid')
                        const order = payload.object
                        debug(`Order -> ${common.viewJSON(order)}`)
                        let payment
                        if (order
                            && order.paymentInfo
                            && order.paymentInfo.payments
                            && _.size(order.paymentInfo.payments)
                            && _.isEqual(_.size(order.paymentInfo.payments), 1)) {
                                payment = order.paymentInfo.payments[0].obj
                        }
                        debug(`Payment -> ${common.viewJSON(payment)}`)
                        if (payment
                            && payment.paymentMethodInfo
                            && payment.paymentMethodInfo.method) {
                                if (_.includes(ALLOWED_PAYMENT_STATE_CHANGED, payment.paymentMethodInfo.method)) {
                                    await updatePaymentTransactionState(payment, token)
                                    payload.object = await getUpdatedOrder(order, token)
                                    payload.channel = getChannel(payload.object)
                                    await common.postData(SNS,
                                        ANALYTICS_ACH_PAID_SNS,
                                        payload,
                                        false)
                                }
                        }
                }
        }
    }
    // Return Info Added
    if (_.isEqual(data.actionType, ORDER_RETURN_INFO_ADDED)) {
        info('Return Info Added')
        await common.postData(SNS,
            ORDER_SNS,
            constructReturnAnalyticsPayload(data),
            false)
        const netsuitePayload = await constructNetsuitePayload(data, token)
        await common.postData(SQS,
            NETSUITE_ORDER_SQS,
            netsuitePayload,
            false)
    }
    // Return Shipment Changed
    // if (_.isEqual(data.actionType, ORDER_RETURN_SHIPMENT_CHANGEDn)) {
    //     info('Return Shipment Changed')
    //     const payload = constructReturnShipmentPayload(data)
    // }
}

const getOrder = async (data, token) => {
    const url = `${CT_ORDER}/${data.objectReference}?expand=state.id&expand=lineItems[*].state[*].state.id&expand=paymentInfo.payments[*].id`
    const order = await common.getObject(url, token)
    return order
}

const getStates = async (data, token) => {
    const url = `${CT_STATES}`
    const states = await common.getObject(url, token)
    return (states && _.size(states.results))
            ? states.results
            : []
}

const handle = async (data, token) => {
    info('Handle Order')
    debug(`Payload -> ${common.viewJSON(data)}`)
    try {
        data.object = await getOrder(data, token)
        data.states = await getStates(data, token)
        await handleOrderTrigger(data, token)
    } catch (error) {
        await common.handleError(error, data)
    }
}

module.exports = {
  handle,
}
