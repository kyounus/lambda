/* eslint-disable no-param-reassign */
const _ = require('lodash')
const { info, debug } = require('../../constants/console')
const common = require('./common')
const {
    CT_PRODUCT,
} = require('../../constants/httpUri')
const {
    SNS,
    ALLOWED_PAYLOAD_ATTRIBUTES,
} = require('../../constants/message')

const {
    PRODUCT_SNS,
} = process.env
require('custom-env').env(true)

const handle = async (data, token) => {
    info('Handle Product')
    debug(`Payload -> ${common.viewJSON(data)}`)
    try {
        if (data && data.objectReference) {
            const url = `${CT_PRODUCT}/${data.objectReference}`
            const product = await common.getObject(url, token)
            data.object = product
            data = _.pick(data, ALLOWED_PAYLOAD_ATTRIBUTES)
            await common.postData(SNS, PRODUCT_SNS, data, false)
        }
    } catch (error) {
        await common.handleError(error, data)
    }
}

module.exports = {
  handle,
}
