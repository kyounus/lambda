# Description
This Lambda will read DynamoDB and process message to Queue.

## LINT and Unit-Testing
1. npm install --save-dev
2. npm run lint
3. npm run test

## Code Quality & Coverage
* SonarQube - http://35.182.7.195:9000/ (TODO)

## Installation

### LOCAL
1. npm install
2. npm i -g serverless
3. sls offline

### DEV/QA/STAGE/PROD
env - dev/qa/stage/prod (TODO)

### Packaging
1. npm run package

## Environment Variable
### Lambda
* .env.example

## Invoked By
### EVENT
* CloudWatch Event

## Executed Service
### DynamoDB
* [NETSUITE_ORDER_STATUS_{env}]

###  SQS
* [NETSUITE_ORDER_REPROCESS_{env}]

### SNS
* [EXCEPTION_{env}]

## Log Location
* AWS Log ARN - arn:aws:logs:[aws-region]:[aws-account]:log-group:/aws/lambda/netsuite_order_picker:*

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
