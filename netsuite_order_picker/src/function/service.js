/* eslint-disable max-len */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
const _ = require('lodash')
const { info, debug, errorInfo } = require('../constants/console')
const repository = require('./repository')

const {
    SNS,
    SQS,
} = require('../constants/message')

const {
    NETSUITE_ORDER_REPROCESS_SQS,
 } = process.env
require('custom-env').env(true)

const viewJSON = (data) => JSON.stringify(data)

const postData = async (type, entityName, data, aliasEnabled) => {
    if (_.isEqual(type, SNS)) {
        debug(`Payload sent to ${SNS} ${entityName} -> ${viewJSON(data)}`)
        await repository.snsMessage(entityName, data, aliasEnabled)
    } else if (_.isEqual(type, SQS)) {
        debug(`Payload sent to ${SQS} ${entityName} -> ${viewJSON(data)}`)
        await repository.sqsMessage(entityName, data, aliasEnabled)
    }
}

const constructPayload = (order) => ({
        orderNumber: order.orderNumber,
        payload: JSON.parse(order.payload),
        status: order.status,
})

const publishMessages = async (orders) => {
    for await (const order of orders) {
        debug(`Order -> ${viewJSON(order)}`)
        try {
            const payload = constructPayload(order)
            await postData(SQS, NETSUITE_ORDER_REPROCESS_SQS, payload, false)
        } catch (error) {
            errorInfo(`Error -> ${error}`)
        }
    }
}

const getEligibleOrders = async () => {
    let orders = []
    try {
        orders = await repository.scanOrders()
    } catch (error) {
        errorInfo(`Error -> ${error}`)
    }
    return orders
}

const handleNetsuiteOrders = async () => {
    info('Handle Netsuite Orders')
    const orders = await getEligibleOrders()
    await publishMessages(orders)
}

module.exports = {
    handleNetsuiteOrders,
}
