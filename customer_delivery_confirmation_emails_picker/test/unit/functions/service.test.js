const _ = require('lodash')
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);
const service = require('./../../../src/function/service')
const repository = require('./../../../src/function/repository')
const CTService = require('./../../../src/service/CTService')
let create = require('./data/create.json')
let update = require('./data/update.json')
let error = require('./data/error.json')
let ctToken = require('./data/token.json')
let ctResponse = require('./data/get_products_res.json')
let productTypeRes = require('./data/get_product_type_res.json')
let ctErrorResponse = require('./data/response(Error1).json')
let ctErrorResponse2 = require('./data/response(Error2).json')
var { HTTPS } = require('../../../src/constants/protocol')
var { CT_PRODUCT } = require('../../../src/constants/httpUri')
const nock = require('nock');
const { get } = require('lodash');

describe('Service', () => {
    describe('processMessage', () => {
        var eventObject
        it('should handle Message - Create Product Flow', async () => {
            const getToken = sinon.stub(CTService, 'getToken')
            const getEntity = sinon.stub(CTService, 'getEntity')
            const createEntity = sinon.stub(CTService, 'createEntity')
            const snsMessage = sinon.stub(repository, 'snsMessage')
            const sqsMessage = sinon.stub(repository, 'sqsMessage')
            getToken.returns(ctToken)
            getEntity.returns(ctResponse)
            createEntity.returns(ctResponse)
            snsMessage.returns({})
            sqsMessage.returns({})
            var response
            try {
                console.log('esaki create', create)
                response = await service.processMessage(create)
                getToken.restore()
                getEntity.restore()
                createEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)    
            } catch (error) {
                console.log(error)
                getToken.restore()
                getEntity.restore()
                createEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)
            }
        })
        it('should handle Message - Update Product Flow', async () => {
            const getToken = sinon.stub(CTService, 'getToken')
            const getEntity = sinon.stub(CTService, 'getEntity')
            const createEntity = sinon.stub(CTService, 'createEntity')
            const snsMessage = sinon.stub(repository, 'snsMessage')
            const sqsMessage = sinon.stub(repository, 'sqsMessage')
            getToken.returns(ctToken)
            getEntity.returns(productTypeRes)
            createEntity.returns(ctResponse)
            snsMessage.returns({})
            sqsMessage.returns({})
            var response
            try {
                response = await service.processMessage(update)
                getToken.restore()
                getEntity.restore()
                createEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)    
            } catch (error) {
                console.log(error)
                getToken.restore()
                getEntity.restore()
                createEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)
            }
        })

        it('should handle Message - Error 1 - No Product', async () => {
            const getToken = sinon.stub(CTService, 'getToken')
            const getEntity = sinon.stub(CTService, 'getEntity')
            const createEntity = sinon.stub(CTService, 'createEntity')
            const snsMessage = sinon.stub(repository, 'snsMessage')
            const sqsMessage = sinon.stub(repository, 'sqsMessage')
            getToken.returns(ctToken)
            getEntity.returns(ctErrorResponse)
            createEntity.returns(ctErrorResponse)
            snsMessage.returns({})
            sqsMessage.returns({})
            var response
            try {
                response = await service.processMessage(error)
                getToken.restore()
                getEntity.restore()
                createEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)    
            } catch (error) {
                console.log(error)
                getToken.restore()
                getEntity.restore()
                createEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)
            }
            sinon.assert.pass()
        })

        it('should handle Message - Error 1 - No Product', async () => {
            const getToken = sinon.stub(CTService, 'getToken')
            const getEntity = sinon.stub(CTService, 'getEntity')
            const createEntity = sinon.stub(CTService, 'createEntity')
            const snsMessage = sinon.stub(repository, 'snsMessage')
            const sqsMessage = sinon.stub(repository, 'sqsMessage')
            getToken.returns(ctToken)
            getEntity.returns(ctErrorResponse2)
            createEntity.returns(ctResponse)
            snsMessage.returns({})
            sqsMessage.returns({})
            var response
            try {
                response = await service.processMessage(update)
                getToken.restore()
                getEntity.restore()
                createEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)    
            } catch (error) {
                console.log(error)
                getToken.restore()
                getEntity.restore()
                createEntity.restore()
                snsMessage.restore()
                sqsMessage.restore()
                console.log(response)
            }
            sinon.assert.pass()
        })

        before(() => {
            nock(`${HTTPS}://${process.env.CT_SERVICE_URL}`)
            .post(`/${process.env.CT_PROJECT_KEY}${CT_PRODUCT}`)
            .reply(200, ctResponse)
            eventObject = {
                Records: [
                    {
                        Sns: JSON.stringify({
                            Message: JSON.stringify(create)
                        })
                    }
                ]
            }
            process.env.UNIT_TEST = 'true'
        })

        after(() => {
            eventObject = {}
            delete process.env.UNIT_TEST
        })
    })
})