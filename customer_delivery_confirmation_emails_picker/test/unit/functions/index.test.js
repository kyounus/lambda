const _ = require('lodash')
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);
const index = require('./../../../src/function/index')
const service = require('./../../../src/function/service')

describe('Index', () => {
    var eventObject
    describe('Handler', () => {
        it('should handle Message - happy Flow', async () => {
            let processOrders = sinon.stub(service, 'processMessage')
            processOrders.returns({})
            const response = await index.handler({
                "Records": [
                    {
                        "body": '{\"ProductType\":\"New\",\"SKU\":\"Test NS CT CT 3\",\"InternalId\":\"112063830112\",\"PurchaseDescription\":\"Test NS CT Air (lirik) 2\",\"Class\":\"Filters\",\"Status\":\"175\",\"Price\":\"175.00\",\"Weight\":\"175.00\",\"UOM\":\"175\",\"UPCCode\":\"175\",\"B2CChannel\":\"F\",\"D2CChannel\":\"F\"}'
                    }
                ]
            })
            processOrders.restore()
            sinon.assert.match(response, {})
        })

        it('should handle error scenario', async () => {
            const processOrders = sinon.stub(service, 'processMessage')
            processOrders.throwsException('Service Down')
            await index.handler(eventObject).catch(error => {
                processOrders.restore()
                sinon.assert.match(_.trim(error), 'Service Down')
            })
            processOrders.restore()
        })
    })

    before(() => {
        eventObject= {
            Records: [
                {
                    Sns: {
                        Message: JSON.stringify({
                            data: "data"
                        })
                    }
                }
            ]
        }
        process.env.UNIT_TEST = 'true'
    })
    
    after(() => {
        eventObject = {}
        delete process.env.UNIT_TEST
    })

})