module.exports = {
  Class: 'sellable',
  Status: 'status',
  Weight: 'weight',
  UOM: 'unitOfMeasure',
  UPCCode: 'upcCode',
  D2CChannel: 'b2cView',
  B2BChannel: 'b2bView',
}
