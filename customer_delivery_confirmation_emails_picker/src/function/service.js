const _ = require('lodash')
const moment = require('moment')
const { info, debug, errorInfo } = require('../constants/console')
const repository = require('./repository')
const CTService = require('../service/CTService')
const {
  CT_CUSTOM_OBJECT,
  CT_ORDER,

} = require('../constants/httpUri')

const {
  SQS,
} = require('../constants/message')

const {
  EMAIL_TRIGGER_DELIVERY_CONFIRMATION_SQS,
  REMINDER_DAYS,
  EMAIL_FROM_ID,
  EMAIL_FROM_NAME,
  TEMPLATE_ID,
  ACCOUNT_LOGIN_URL,
} = process.env

const viewJSON = (data) => JSON.stringify(data)

const constructPayload = (orderDetails) => {
  const { shippingAddress, customerEmail } = orderDetails
  const payload = {
    from: {
      email: EMAIL_FROM_ID,
      name: EMAIL_FROM_NAME,
    },
    personalizations: [
      {

        to:
          [{
            email: customerEmail,
            name: shippingAddress.firstName + shippingAddress.lastName,
          }],

        dynamic_template_data: {
          customerFirstName: shippingAddress.firstName,
          customerLastName: shippingAddress.lastName,
          customerStreetOne: shippingAddress.streetName,
          customerStreetTwo: (shippingAddress.streetNumber) ? shippingAddress.streetNumber : '',
          customerCity: shippingAddress.city,
          customerState: shippingAddress.state,
          customerZip: shippingAddress.postalCode,
          customerCountry: shippingAddress.country,
          accountLoginUrl: ACCOUNT_LOGIN_URL,
          promo: true,
        },
      },
    ],
    template_id: TEMPLATE_ID,
  }

  return payload
}

const postData = async (type, entityName, data, aliasEnabled) => {
  if (_.isEqual(type, SQS)) {
    debug(`Payload sent to ${SQS} ${entityName} -> ${viewJSON(data)}`)
    await repository.sqsMessage(entityName, data, aliasEnabled)
  }
}

const getSubscriptions = async (token) => {
  debug('Get subscriptions')
  const dateToday = moment().format('YYYY-MM-DD')
  const newDate = moment(dateToday, 'YYYY-MM-DD').add(REMINDER_DAYS, 'days')
  const nextOrderDate = moment(newDate).format('YYYY-MM-DD')
  const messages = []
  const nexOrderUrl = `nextOrderDate="${nextOrderDate}"`

  const encodedUrl = encodeURIComponent(nexOrderUrl)

  const subscription = `?limit=500&expand=value.cart.id&where=value(subscription%3Dtrue)&where=value(${encodedUrl})`

  const subscriptionUrl = `${CT_CUSTOM_OBJECT}/${subscription}`
  const subscriptionData = await CTService.getEntity(subscriptionUrl, token)
  if (subscriptionData && subscriptionData.results) {
    for await (const record of subscriptionData.results) {
      try {
        const orderId = record.value.cart.obj.custom.fields.parentOrderReferenceId.id
        const orderUrl = `${CT_ORDER}/${orderId}`
        const orderDetails = await CTService.getEntity(orderUrl, token)
        const payload = constructPayload(orderDetails)
        console.log(JSON.stringify(payload))

        messages.push(payload)
        await postData(SQS, EMAIL_TRIGGER_DELIVERY_CONFIRMATION_SQS, payload, false)
      } catch (error) {
        errorInfo(`Invalid Message format ${error}`)
      }
    }
  }
}

const processMessages = async () => {
  info('Process Messages')
  const token = await CTService.getToken()
  const subscriptions = await getSubscriptions(token)
  return subscriptions
}

const getMessages = (event) => {
  info('Fetch Messages')
  const { Records } = event
  const messages = []
  if (Records) {
    for (const record of Records) {
      try {
        const { body } = record
        const message = JSON.parse(body)
        messages.push(message)
      } catch (error) {
        errorInfo(`Invalid Message format ${error}`)
      }
    }
  }
  debug(`Messages -> ${viewJSON(messages)}`)
  return messages
}

module.exports = {
  getMessages,
  processMessages,
}
