require('custom-env').env('local')
const { info, errorInfo } = require('../constants/console')
const service = require('./service')

const handler = async () => {
  try {
    info('CUSTOMER EMAIL PICKER FOR DELIVERY CONFIRMATION HANDLER')
    await service.processMessages()
    return { }
  } catch (error) {
    errorInfo(error)
    return { }
  }
}

module.exports = {
  handler,
}
