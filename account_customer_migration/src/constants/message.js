module.exports = {
  MESSAGE_1: '',
  MESSAGE_2: '',
  CT_NOTIFICATION_MESSAGE: 'Message',
  CT_NOTIFICATION_CREATED: 'ResourceCreated',
  CT_NOTIFICATION_UPDATED: 'ResourceUpdated',
  CT_TYPE_CUSTOMER_CREATED: 'CustomerCreated',
  CT_RESOURCE_CUSTOMER: 'customer',
  CT_RESOURCE_CUSTOM_OBJECT: 'key-value-document',
  CT_B2B_CONTAINER: 'b2b',
  MIGRATION_FAILED: 'Account / Customer migration Failed',
}
