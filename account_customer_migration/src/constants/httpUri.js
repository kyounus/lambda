module.exports = {
  CT_OAUTH_TOKEN: '/oauth/token',
  CT_CUSTOMER: '/customers',
  CT_CUSTOM_OBJECT: '/custom-objects',
  CT_ORDER: '/orders',
}
