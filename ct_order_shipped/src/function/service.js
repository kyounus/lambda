/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
const _ = require('lodash')
const { info, debug, errorInfo } = require('../constants/console')
const repository = require('./repository')
const {
    SNS,
    SQS,
    CT_RESOURCE_ORDER,
    ALLOWED_ORDER_ACTION_TYPE,
    ALLOWED_ORDER_ACTION,
} = require('../constants/message')

const {
    INVOICE_ORDER_SQS,
    SUBSCRIPTION_PLAN_UPDATE_SQS,
} = process.env
require('custom-env').env(true)

const viewJSON = (data) => JSON.stringify(data)

const postData = async (type, entityName, data, aliasEnabled) => {
    if (_.isEqual(type, SNS)) {
        debug(`Payload sent to ${SNS} ${entityName} -> ${viewJSON(data)}`)
        await repository.snsMessage(entityName, data, aliasEnabled)
    } else if (_.isEqual(type, SQS)) {
        debug(`Payload sent to ${SQS} ${entityName} -> ${viewJSON(data)}`)
        await repository.sqsMessage(entityName, data, aliasEnabled)
    }
}

const transform = async (data) => {
    info('Transform Data')
    await postData(SQS,
        INVOICE_ORDER_SQS,
        data,
        false)
    await postData(SQS,
        SUBSCRIPTION_PLAN_UPDATE_SQS,
        data,
        false)
}

const validate = (data) => {
    let isValidData = false
    info('Validate Message')
    if (data
        && data.action
        && data.actionType
        && data.entity
        && data.object) {
            if (_.includes(ALLOWED_ORDER_ACTION, data.action)
                && _.includes(ALLOWED_ORDER_ACTION_TYPE, data.actionType)
                && _.isEqual(CT_RESOURCE_ORDER, data.entity)) {
                    isValidData = true
            }
    }
    (isValidData)
        ? debug(`Message Picked -> ${viewJSON(data)}`)
        : debug(`Message Not-Picked -> ${viewJSON(data)}`)
    return isValidData
}

const processMessages = async (messages) => {
    info('Process Messages')
    for await (const message of messages) {
        debug(`Message -> ${message}`)
        if (validate(message)) {
            await transform(message)
        }
    }
}

const getMessages = (event) => {
    info('Fetch Messages')
    const { Records } = event
    const messages = []
    if (Records) {
        for (const record of Records) {
            try {
                const { body } = record
                const message = JSON.parse(body)
                messages.push(message)
            } catch (error) {
                errorInfo(`Invalid Message format ${error}`)
            }
        }
    }
    debug(`Messages -> ${viewJSON(messages)}`)
    return messages
}

module.exports = {
    getMessages,
    processMessages,
}
