/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
const _ = require('lodash')
const { info, debug, errorInfo } = require('../constants/console')
const CTService = require('../service/CTService')
const repository = require('./repository')
const {
    CT_ORDER,
} = require('../constants/httpUri')
const {
    SNS,
    SQS,
    CT_RESOURCE_ORDER,
    SET_LINEITEM_CUSTOM_FIELD,
    INVOICE_URL_FIELD,
    ALLOWED_PAYLOAD_ATTRIBUTES,
    ALLOWED_ORDER_ACTION_TYPE,
    ALLOWED_ORDER_ACTION,
} = require('../constants/message')

const {
    ANALYTICS_ORDER_SNS,
    INVOICE_URL,
} = process.env
require('custom-env').env(true)

const viewJSON = (data) => JSON.stringify(data)

const postData = async (type, entityName, data, aliasEnabled) => {
    if (_.isEqual(type, SNS)) {
        debug(`Payload sent to ${SNS} ${entityName} -> ${viewJSON(data)}`)
        await repository.snsMessage(entityName, data, aliasEnabled)
    } else if (_.isEqual(type, SQS)) {
        debug(`Payload sent to ${SQS} ${entityName} -> ${viewJSON(data)}`)
        await repository.sqsMessage(entityName, data, aliasEnabled)
    }
}

const getChannel = (object) => {
    let channel = 'B2B'
    if (object
        && object.custom
        && object.custom.fields
        && object.custom.fields.channel) {
            channel = object.custom.fields.channel
    }
    return channel
}

const constructAnalyticsPayload = (data, order) => {
    data.object = order
    const payload = _.pick(data, ALLOWED_PAYLOAD_ATTRIBUTES)
    payload.channel = getChannel(payload.object)
    return payload
}

const analytics = async (data, order) => {
    // Analytics
    if (order
        && order.id) {
            await postData(SNS,
                ANALYTICS_ORDER_SNS,
                constructAnalyticsPayload(data, order),
                false)
    }
}

const getObject = async (url, token) => {
    info('Get Object')
    const object = await CTService.getEntity(url, token)
    debug(`Object -> ${viewJSON(object)}`)
    return object
}

const getOrder = async (data, token) => {
    const order = data.object
    const url = `${CT_ORDER}/${order.id}?expand=state.id&expand=lineItems[*].state[*].state.id`
    const latestOrder = await getObject(url, token)
    return latestOrder
}

const constructUpdateOrderPayload = (data, updatedOrder) => {
    const order = data.object
    const { lineItems } = order
    const item = _.filter(lineItems, (lineItem) => _.isEqual(lineItem.id, data.lineItemId))
    const { orderNumber } = updatedOrder
    const { itemFulfillmentNumber } = item[0].custom.fields
    debug(`OrderNumber -> ${orderNumber}`)
    debug(`itemFulfillmentNumber -> ${itemFulfillmentNumber}`)
    debug(`lineItemId -> ${item[0].id}`)
    debug(`Invoice Url -> ${INVOICE_URL}/${orderNumber}-${itemFulfillmentNumber}.pdf`)
    return {
        version: updatedOrder.version,
        actions: [
            {
                action: SET_LINEITEM_CUSTOM_FIELD,
                lineItemId: item[0].id,
                name: INVOICE_URL_FIELD,
                value: `${INVOICE_URL}/${orderNumber}-${itemFulfillmentNumber}.pdf`,
            },
        ],
    }
}

const updateOrder = async (data, token) => {
    const order = await getOrder(data, token)
    const payload = constructUpdateOrderPayload(data, order)
    debug(`Payload -> ${viewJSON(payload)}`)
    const url = `${CT_ORDER}/${order.id}?expand=state.id&expand=lineItems[*].state[*].state.id&expand=paymentInfo.payments[*].id`
    const updatedOrder = await CTService.createEntity(url, payload, token)
    return updatedOrder
}

const verifyOrder = (data) => {
    let flag = false
    const order = data.object
    const { orderNumber, lineItems } = order
    const items = _.filter(lineItems, (lineItem) => _.isEqual(lineItem.id, data.lineItemId))
    if (orderNumber
        && items
        && _.size(items)) {
            const item = items[0]
            if (item.custom
                && item.custom.fields
                && item.custom.fields.itemFulfillmentNumber) {
                    flag = true
                }
    }
    return flag
}

const transform = async (data, token) => {
    info('Transform Data')
    if (verifyOrder(data)) {
        const updatedOrder = await updateOrder(data, token)
        await analytics(data, updatedOrder)
    }
}

const validate = (data) => {
    let isValidData = false
    info('Validate Message')
    if (data
        && data.action
        && data.actionType
        && data.entity
        && data.object) {
            if (_.includes(ALLOWED_ORDER_ACTION, data.action)
                && _.includes(ALLOWED_ORDER_ACTION_TYPE, data.actionType)
                && _.isEqual(CT_RESOURCE_ORDER, data.entity)) {
                    isValidData = true
            }
    }
    (isValidData)
        ? debug(`Message Picked -> ${viewJSON(data)}`)
        : debug(`Message Not-Picked -> ${viewJSON(data)}`)
    return isValidData
}

const processMessages = async (messages) => {
    info('Process Messages')
    const token = await CTService.getToken()
    for await (const message of messages) {
        debug(`Message -> ${message}`)
        if (validate(message)) {
            await transform(message, token)
        }
    }
}

const getMessages = (event) => {
    info('Fetch Messages')
    const { Records } = event
    const messages = []
    if (Records) {
        for (const record of Records) {
            try {
                const { body } = record
                const message = JSON.parse(body)
                messages.push(message)
            } catch (error) {
                errorInfo(`Invalid Message format ${error}`)
            }
        }
    }
    debug(`Messages -> ${viewJSON(messages)}`)
    return messages
}

module.exports = {
    getMessages,
    processMessages,
}
