const _ = require('lodash')
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);
const requestManager = require('../../../src/handler/requestManager')
const ctService = require('../../../src/service/CTService')

describe('CT Service', () => {
    describe('Get Token', () => {
        it('should get entity', async () => {
            const httpRequest = sinon.stub(requestManager, 'httpRequest')
            httpRequest.returns({
                access_token: "wbELHMKHNntYs5n02lkNQSH084uWAr6G",
                token_type: "Bearer",
                expires_in: 172315,
                scope: "manage_project:mo-dev"
            })
            const response = await ctService.getToken()
            httpRequest.restore()
            sinon.assert.match(response.access_token, "wbELHMKHNntYs5n02lkNQSH084uWAr6G")
        })
        it('should throw error', async () => {
            const httpRequest = sinon.stub(requestManager, 'httpRequest')
            httpRequest.throwsException("Service Down")
            const response = await ctService.getToken()
            httpRequest.restore()
            sinon.assert.match(_.trim(response), "Service Down")
        })
    })

    describe('Create Entity', () => {
        it('should create entity', async () => {
            const httpRequest = sinon.stub(requestManager, 'httpRequest')
            httpRequest.returns({
                id: "9e84485f-1229-489c-ad20-c7bd8a15ada4"
            })
            const response = await ctService.createEntity('/cart', 
            {id: 'test-cart'},
            {
                access_token: "wbELHMKHNntYs5n02lkNQSH084uWAr6G",
                token_type: "Bearer",
                expires_in: 172315,
                scope: "manage_project:mo-dev"
            })
            httpRequest.restore()
            sinon.assert.match(response.id, "9e84485f-1229-489c-ad20-c7bd8a15ada4")
        })
        it('should throuw error', async () => {
            const httpRequest = sinon.stub(requestManager, 'httpRequest')
            httpRequest.throwsException("Service Down")
            const response = await ctService.createEntity('/cart',
            {id: 'test-cart'},
            {
                access_token: "wbELHMKHNntYs5n02lkNQSH084uWAr6G",
                token_type: "Bearer",
                expires_in: 172315,
                scope: "manage_project:mo-dev"
            }
            )
            httpRequest.restore()
            sinon.assert.match(_.trim(response), "Service Down")
        })
    })

    describe('Get Entity', () => {
        it('should get entity', async () => {
            const httpRequest = sinon.stub(requestManager, 'httpRequest')
            httpRequest.returns({
                id: "9e84485f-1229-489c-ad20-c7bd8a15ada4"
            })
            const response = await ctService.getEntity('/cart/test-cart',
            {
                access_token: "wbELHMKHNntYs5n02lkNQSH084uWAr6G",
                token_type: "Bearer",
                expires_in: 172315,
                scope: "manage_project:mo-dev"
            })
            httpRequest.restore()
            sinon.assert.match(response.id, "9e84485f-1229-489c-ad20-c7bd8a15ada4")
        })
        it('should throuw error', async () => {
            const httpRequest = sinon.stub(requestManager, 'httpRequest')
            httpRequest.throwsException("Service Down")
            const response = await ctService.getEntity('/cart/test-cart',
            {
                access_token: "wbELHMKHNntYs5n02lkNQSH084uWAr6G",
                token_type: "Bearer",
                expires_in: 172315,
                scope: "manage_project:mo-dev"
            })
            httpRequest.restore()
            sinon.assert.match(_.trim(response), "Service Down")
        })
    })
})