const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);
const repository = require('../../../src/function/repository')
const sqs = require('../../../src/handler/sqs')
const sns = require('../../../src/handler/sns')

describe('Repository', () => {
    
    describe('Invoker', () => {
        // SQS Message
        it('should messgae sqs', async () => {
            const sqsHandler = sinon.stub(sqs, 'message')
            sqsHandler.returns(JSON.stringify({
                statusCode : 200
            }))
            await repository.sqsMessage('test-sqs', {}, (error, response) => {
                sqsHandler.restore()
                sinon.assert.match(response.statusCode, 200)
            })
            sqsHandler.restore()
        })
        // SNS Message
        it('should messgae sns', async () => {
            const snsHandler = sinon.stub(sns, 'message')
            snsHandler.returns(JSON.stringify({
                statusCode : 200
            }))
            await repository.snsMessage('test-sns', {}, (error, response) => {
                snsHandler.restore()
                sinon.assert.match(response.statusCode, 200)
            })
            snsHandler.restore()
        })
    })

    before(() => {
        process.env.LOG_MODE = 'INFO'
    })
    
    after(() => {
        process.env.LOG_MODE = 'DEBUG'
    })
})