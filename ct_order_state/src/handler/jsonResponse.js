function json(data, headers = {}) {
  const { statusCode } = data
  return {
    statusCode,
    headers: {
      'Access-Control-Allow-Origin': process.env.ALLOW_ORIGIN,
      'Access-Control-Allow-Credentials': true,
      ...headers,
    },
    body: data.body,
  }
}

module.exports = {
  json,
}
