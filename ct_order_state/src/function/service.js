/* eslint-disable max-len */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
const _ = require('lodash')
const { info, debug, errorInfo } = require('../constants/console')
const CTService = require('../service/CTService')

const {
    CT_ORDER,
    CT_PRODUCT_TYPE,
    CT_STATES,
} = require('../constants/httpUri')
const {
    ACTION_TRANSITION_STATE,
    ACTION_CHANGE_STATE,
    STATE,
    COMPLETE,
    LINEITEM_STATE_SHIPPED,
    LINEITEM_STATE_BACKORDERED,
    ORDER_STATE_SHIPPED,
    ORDER_STATE_BACKORDERED,
    CT_RESOURCE_ORDER,
    ALLOWED_ORDER_ACTION_TYPE,
    ALLOWED_ORDER_ACTION,
} = require('../constants/message')

require('custom-env').env(true)

const viewJSON = (data) => JSON.stringify(data)

const productLineItem = async (order, token) => {
    const products = []
    const url = `${CT_PRODUCT_TYPE}/key=subscription`
    const subscriptionType = await CTService.getEntity(url, token)
    for await (const lineItem of order.lineItems) {
        if (subscriptionType
            && subscriptionType.id
            && lineItem
            && lineItem.productType
            && lineItem.productType.id) {
                if (!_.isEqual(subscriptionType.id, lineItem.productType.id)) {
                    debug(`Product Line Item -> ${lineItem.productType.id}`)
                    products.push(lineItem)
                }
        }
    }
    return products
}

const getStates = async (token) => {
    const url = `${CT_STATES}`
    const states = await CTService.getEntity(url, token)
    return (states && _.size(states.results))
            ? states.results
            : []
}

const getState = (states, stateKey) => _.find(states, (state) => _.isEqual(state.key, stateKey))

const handleOrder = async (data, token) => {
    info('Handle Order')
    let shippedFlag = false
    let backorderedFlag = false
    const order = data.object
    const states = await getStates(token)
    const shippedState = getState(states, LINEITEM_STATE_SHIPPED)
    const backorderedState = getState(states, LINEITEM_STATE_BACKORDERED)
    debug(`States -> ${viewJSON(states)}`)
    debug(`Shipped State -> ${viewJSON(shippedState)}`)
    debug(`Backordered State -> ${viewJSON(backorderedState)}`)
    if (shippedState && backorderedState) {
        const lineItems = await productLineItem(order, token)
        let masterShipped = 0
        let masterBackordered = 0
        let masterQuantity = 0
        for (const lineItem of lineItems) {
            const { quantity } = lineItem
            let stateQuantity = 0
            masterQuantity += quantity
            for (const state of lineItem.state) {
                stateQuantity += state.quantity
            }
            if (_.isEqual(quantity, stateQuantity)) {
                debug('Line-Item and Quantity are Same')
                const shipped = _.filter(lineItem.state, (state) => _.isEqual(state.state.id, shippedState.id))
                const backordered = _.filter(lineItem.state, (state) => _.isEqual(state.state.id, backorderedState.id))
                debug(`Shipped Line-Item State -> ${viewJSON(shipped)}`)
                let stateShippedQuantity = 0
                for (const ship of shipped) {
                    stateShippedQuantity += ship.quantity
                }
                masterShipped += stateShippedQuantity
                debug(`Backordered Line-Item State -> ${viewJSON(backordered)}`)
                let stateBackorderedQuantity = 0
                for (const back of backordered) {
                    stateBackorderedQuantity += back.quantity
                }
                masterBackordered += stateBackorderedQuantity
            }
        }
        debug(`Master Shipped Count -> ${masterShipped}`)
        debug(`Master Backordered Count -> ${masterBackordered}`)
        debug(`Master Quantity Count -> ${masterQuantity}`)
        if (_.isEqual(masterShipped, masterQuantity)) {
            shippedFlag = true
        } else if (_.isEqual(masterBackordered, masterQuantity)) {
            backorderedFlag = true
        }
    }
    return {
        shipped: shippedFlag,
        backordered: backorderedFlag,
    }
}

const getOrder = async (data, token) => {
    const url = `${CT_ORDER}/${data.id}`
    const order = await CTService.getEntity(url, token)
    debug(`Live Order -> ${viewJSON(order)}`)
    return order
}

const constructUpdateOrder = async (order, state, token) => {
    debug('Change Order State')
    const liveOrder = await getOrder(order, token)
    debug(`Live Order Version -> ${liveOrder.version}`)
    const payload = {
        version: liveOrder.version,
        actions: [
            {
                action: ACTION_TRANSITION_STATE,
                state: {
                  typeId: STATE,
                  key: state,
                },
            },
        ],
    }
    if (_.isEqual(state, ORDER_STATE_SHIPPED)) {
        payload.actions.push({
            action: ACTION_CHANGE_STATE,
            orderState: COMPLETE,
        })
    }
    return payload
}

const updateOrder = async (order, state, token) => {
    debug('Update Order')
    const payload = await constructUpdateOrder(order, state, token)
    debug(`Update Payload -> ${viewJSON(payload)}`)
    const url = `${CT_ORDER}/${order.id}`
    const updatedOrder = await CTService.createEntity(url, payload, token)
    debug(`Updated Order -> ${viewJSON(updatedOrder)}`)
}

const transform = async (data, token) => {
    info('Transform Data')
    const order = data.object
    const { shipped, backordered } = await handleOrder(data, token)
    // Update order
    if (shipped) {
        await updateOrder(order, ORDER_STATE_SHIPPED, token)
    } else if (backordered) {
        await updateOrder(order, ORDER_STATE_BACKORDERED, token)
    }
}

const eligibleOrder = async (data, token) => {
    const { shipped, backordered } = await handleOrder(data, token)
    return ((shipped || backordered))
}

const validate = async (data, token) => {
    let isValidData = false
    info('Validate Message')
    if (data
        && data.action
        && data.actionType
        && data.entity
        && data.object
        && data.fromState
        && data.toState
        && data.quantity) {
            if (_.includes(ALLOWED_ORDER_ACTION, data.action)
                && _.includes(ALLOWED_ORDER_ACTION_TYPE, data.actionType)
                && _.isEqual(CT_RESOURCE_ORDER, data.entity)) {
                    if (await eligibleOrder(data, token)) {
                        isValidData = true
                    }
            }
    }
    (isValidData)
        ? debug(`Message Picked -> ${viewJSON(data)}`)
        : debug(`Message Not-Picked -> ${viewJSON(data)}`)
    return isValidData
}

const processMessages = async (messages) => {
    info('Process Messages')
    const token = await CTService.getToken()
    for await (const message of messages) {
        debug(`Message -> ${message}`)
        if (await validate(message, token)) {
            await transform(message, token)
        }
    }
}

const getMessages = (event) => {
    info('Fetch Messages')
    const { Records } = event
    const messages = []
    if (Records) {
        for (const record of Records) {
            try {
                const { body } = record
                const message = JSON.parse(body)
                messages.push(message)
            } catch (error) {
                errorInfo(`Invalid Message format ${error}`)
            }
        }
    }
    debug(`Messages -> ${viewJSON(messages)}`)
    return messages
}

module.exports = {
    getMessages,
    processMessages,
}
