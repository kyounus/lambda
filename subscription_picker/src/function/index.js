require('custom-env').env(true)
const { info, debug, errorInfo } = require('../constants/console')
const service = require('./service')

const handler = async (event) => {
  try {
    info('SUBSCRIPTION PICKER - handler')
    debug(`Event Received -> ${JSON.stringify(event)}`)
    await service.pickSubscription()
    return { }
  } catch (error) {
    errorInfo(error)
    return {}
  }
}

module.exports = {
  handler,
}
