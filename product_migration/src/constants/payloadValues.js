module.exports = {
    SKU: 'SKU',
    PURCHASEDESCRIPTION: 'PurchaseDescription',
    PRICE: 'Price',
    PRODUCTTYPE: 'product-type',
    VARIANTID: 1,
    CURRENCYCODE: 'USD',
    SETSKU: 'setSku',
    SETDESCRIPTION: 'setDescription',
    SETATTRIBUTE: 'setAttribute',
    CHANGEPRICE: 'changePrice',
}
