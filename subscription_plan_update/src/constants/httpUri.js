module.exports = {
  CT_OAUTH_TOKEN: '/oauth/token',
  CT_CUSTOMER: '/customers',
  CT_CUSTOM_OBJECT: '/custom-objects',
  CT_PRODUCT_TYPE: '/product-types',
  CT_PRODUCT: '/products',
  CT_ORDER: '/orders',
  CT_CART: '/carts',
}
