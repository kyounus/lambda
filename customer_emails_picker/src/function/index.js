require('custom-env').env('local')
const { info, errorInfo } = require('../constants/console')
const service = require('./service')

const handler = async () => {
  try {
    info('PRODUCT MIGRATION HANDLER')
    // debug(`Event Received -> ${JSON.stringify(event)}`)
    // debug(`Message Received -> ${event.Records[0].body}`)
    // const messages = service.getMessages(event)
    await service.processMessages()
    return { }
  } catch (error) {
    errorInfo(error)
    return { }
  }
}

module.exports = {
  handler,
}
