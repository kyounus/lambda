const _ = require('lodash')
const moment = require('moment')
const { info, debug, errorInfo } = require('../constants/console')
const repository = require('./repository')
const CTService = require('../service/CTService')
const {
  CT_CUSTOMER,
  CT_CUSTOM_OBJECT,
  CT_ORDER,

} = require('../constants/httpUri')

const {
  SQS,
} = require('../constants/message')

const {
  EMAIL_TRIGGER_SQS,
  REMINDER_DAYS,
  EMAIL_FROM_ID,
  EMAIL_FROM_NAME,
  TEMPLATE_ID,
  ACCOUNT_LOGIN_URL,
} = process.env

// const displayJson = (data) => ((data) ? JSON.stringify(data) : 'Not Available')

const viewJSON = (data) => JSON.stringify(data)

// const handleError = async (error, product) => {
//   errorInfo(MIGRATION_FAILED)
//   errorInfo(`Product -> ${displayJson(product)}`)
//   const snsPayload = {
//       error: MIGRATION_FAILED,
//       payload: {
//           product,
//       },
//   }
//   await repository.snsMessage(EXCEPTION_SNS, snsPayload, false)
//   throw error
// }

const constructPayload = (customerDetails, parameters) => {
  const {
 nextOrderDate, formattedTotal, productName, serialNumber,
} = parameters
  const payload = {
    from: {
      email: EMAIL_FROM_ID,
      name: EMAIL_FROM_NAME,
    },
    personalizations: [
      {

        to:
          [{
            email: customerDetails.email,
            name: customerDetails.firstName + customerDetails.lastName,
          }],

        dynamic_template_data: {
          subscriptionPrice: `$${formattedTotal}`,
          items: [{
            productName,
          }],
          subscriptionDeviceSerial: serialNumber,
          subscriptionRenewalDate: nextOrderDate,
          accountPortalUrl: ACCOUNT_LOGIN_URL,
          promo: true,
        },
      },
    ],
    template_id: TEMPLATE_ID,
  }

  return payload
}

const postData = async (type, entityName, data, aliasEnabled) => {
  if (_.isEqual(type, SQS)) {
    debug(`Payload sent to ${SQS} ${entityName} -> ${viewJSON(data)}`)
    await repository.sqsMessage(entityName, data, aliasEnabled)
  }
}

function addZeroes(num) {
  // Convert input string to a number and store as a variable.
  let value = Number(num)
  // Split the input string into two arrays containing integers/decimals
  const res = num.split('.')
  // If there is no decimal point or only one decimal place found.
  if (res.length === 1 || res[1].length < 3) {
    // Set the number to two decimal places
    value = value.toFixed(2)
  }
  // Return updated or original number.
  return value
}

const getSubscriptions = async (token) => {
  debug('Get subscriptions')
  const dateToday = moment().format('YYYY-MM-DD')
  const newDate = moment(dateToday, 'YYYY-MM-DD').add(REMINDER_DAYS, 'days')
  const nextOrderDate = moment(newDate).format('YYYY-MM-DD')
  const messages = []
  const nexOrderUrl = `nextOrderDate="${nextOrderDate}"`

  const encodedUrl = encodeURIComponent(nexOrderUrl)

  const subscription = `?limit=500&expand=value.cart.id&where=value(subscription%3Dtrue)&where=value(${encodedUrl})`

  const url = `${CT_CUSTOM_OBJECT}/${subscription}`
  const subscriptionData = await CTService.getEntity(url, token)
  if (subscriptionData && subscriptionData.results) {
    for await (const record of subscriptionData.results) {
      try {
        const subscriptionTotal = record.value.cart.obj.totalPrice.centAmount / 100
        const formattedTotal = addZeroes(subscriptionTotal.toString())
        const orderId = record.value.cart.obj.custom.fields.parentOrderReferenceId.id
        const productName = (record.value.product) ? record.value.product.name['en-US'] : record.value.cart.obj.lineItems[0].name['en-US']
        const serialNumber = (record.value.cart.obj.lineItems[0].custom
          && record.value.cart.obj.lineItems[0].custom.fields
          && record.value.cart.obj.lineItems[0].custom.fields.serialNumbers)
          ? record.value.cart.obj.lineItems[0].custom.fields.serialNumbers[0] : ''
        const url2 = `${CT_ORDER}/${orderId}`

        const orderDetails = await CTService.getEntity(url2, token)
        const customerID = orderDetails.customerId
        const url3 = `${CT_CUSTOMER}/${customerID}`
        const customerDetails = await CTService.getEntity(url3, token)
        const payloadParameters = {
 nextOrderDate, formattedTotal, productName, serialNumber,
}
        const payload = constructPayload(customerDetails, payloadParameters)
        console.log(JSON.stringify(payload))

        // const message = JSON.parse(body)
        messages.push(payload)
        await postData(SQS, EMAIL_TRIGGER_SQS, payload, false)
      } catch (error) {
        errorInfo(`Invalid Message format ${error}`)
      }
    }

    // await postData(SQS,EMAIL_TRIGGER_SQS,messages,false)
  }
}

// const sendEmailtoCustomer = async (payload) => {
//   const url = `${CT_SENDGRID_EMAIL}`
//   const token = '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'

//   const res = await CTService.emailEntity(url, payload, token)
//   return res
// }

const processMessages = async () => {
  info('Process Messages')
  const token = await CTService.getToken()
  const subscriptions = await getSubscriptions(token)
  return subscriptions
}

const getMessages = (event) => {
  info('Fetch Messages')
  const { Records } = event
  const messages = []
  if (Records) {
    for (const record of Records) {
      try {
        const { body } = record
        const message = JSON.parse(body)
        messages.push(message)
      } catch (error) {
        errorInfo(`Invalid Message format ${error}`)
      }
    }
  }
  debug(`Messages -> ${viewJSON(messages)}`)
  return messages
}

module.exports = {
  getMessages,
  processMessages,
}
