# Description
This lambda get invoked once Product payload placed into SQS. Netsuite to CT transformation takes place and product created in CommerceTools. On failure payload sent to error SNS

## LINT and Unit-Testing
1. npm install --save-dev
2. npm run lint
3. npm run test

## Code Quality & Coverage
* SonarQube - http://35.182.7.195:9000/ (TODO)

## Installation

### LOCAL
1. npm install
2. npm i -g serverless
3. sls offline

### DEV/QA/STAGE/PROD
env - dev/qa/stage/prod

### Packaging
1. npm run package

## Environment Variable
### Lambda
* CT_AUTH_URL
* CT_SERVICE_URL
* CT_CLIENT_ID
* CT_CLIENT_SECRET
* CT_PROJECT_KEY
* AWS_URL
* AWS_ARN
* AWS_MOLEKULE_REGION
* NETSUITE_PRODUCT_SQS
* EXCEPTION_SNS
* ALIAS
* LOG_MODE

## Invoked By
### SQS
* [CMS_Product_Queue]

## Executed Service
### SNS
* [exception_notification]

## Log Location
* AWS Log ARN - arn:aws:logs:[aws-region]:[aws-account]:log-group:/aws/lambda/product_migration:*


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
